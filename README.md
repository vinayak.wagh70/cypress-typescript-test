# cypress-typescript-test

## Run Test on Gitlab
There is existing [Gitlab schedule](https://gitlab.com/vinayak.wagh70/cypress-typescript-test/-/pipeline_schedules) which can be used to run test in Gitlab.
Execute schedule named `Run Test` from the list.

## Run Test on Local Machine
Test runner is present in the root of the repository, `local_run.sh`

First clone/download the repository from Gutlab.

A docker image can be used to execute the test.
Test can be run using volume mount of your local copy of repository. 
Go to the root of your repository from command prompt & execute following command.

On Mac/Linux:
```
docker run -it --rm --entrypoint /bin/bash -v $PWD:/cypress-typescript-test -w /cypress-typescript-test cypress/browsers:node18.12.0-chrome106-ff106
```

On Windows:
```
docker run -it --rm --entrypoint /bin/bash -v %cd%:/cypress-typescript-test -w /cypress-typescript-test cypress/browsers:node18.12.0-chrome106-ff106
```
Then execute following commands inside running container.
```
npm install
sh local_run.sh
```

Then final report will be generated at location `mochawesome-report/mochawesome.html`.

## Project Structure
- `/cypress`
    - `e2e` - Test suite files
    - `support`
        - `page_objects` - Reusable classes, constants, variables, functions, etc.
- `mochawesome-report` - Final generated Mochawesome report.

#!/usr/bin/env bash

# Execute test
npx cypress run --browser chrome

# Generate Report
npx mochawesome-merge "mochawesome-report/*.json" > mochawesome.json
npx marge mochawesome.json

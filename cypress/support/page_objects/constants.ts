export const PARALLEL_CARD = 'Parallel card';
export const PARALLEL_CARD_MONTHLY_FEE = '3,00 €';
export const NORDEA_CREDIT_MASTERCARD = 'Nordea Credit Mastercard';
export const LANGUAGE_SUOMI = 'Suomi';
export const LANGUAGE_SVENSKA = 'Svenska';
export const LANGUAGE_ENGLISH = 'English';

const SEARCH_LIST = '.search__result-list';
const SEARCH_PAGE = '.search-pagination ul';

export class SearchResult{
    navigateToPageThatContains(key: string){
        cy.get(SEARCH_LIST).then(searchList => {
            cy.wrap(searchList).find('a').then(link => {
                if(!link.text().includes(key)){
                    cy.get(SEARCH_PAGE).contains('2').click();
                }
            })         
        });
    }
}

export const searchResult = new SearchResult();

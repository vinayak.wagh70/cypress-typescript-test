const LANGUAGE_LIST = '[name="Choose language"]';
const NAVIGATION_BOTTOM_BAR = '.nav-bottom';
const SEARCH_BUTTON = '.icon-search';
const SEARCH_FIELD = '[placeholder="Search here"]';
const SUBMIT = '[type="submit"]';

export class Navigation{
    chooseLanguage(language: string){
        cy.get(LANGUAGE_LIST).select(language, { force: true });
    }

    searchItem(key: string){
        cy.get(NAVIGATION_BOTTOM_BAR)
            .find(SEARCH_BUTTON)
            .click({ force: true });
        cy.get(SEARCH_FIELD).type(key, { force: true });
        cy.get(SUBMIT).click({ force: true });
    }
}

export const navigate = new Navigation();

const PRICES = '#Prices';
const TEXT_PRICES_AFTER_1_9_2019 = 'Prices for Nordea Credit cards issued after 1.9.2019';
const HEADER_5 = 'H5';
const TEXT_MONTHLY_FEE = 'Monthly fee';
const ELEMENT_ARTICLE = 'article';

export class NordeaCreditMasterCard{
    showPrices(){
        cy.get(PRICES).click();
    }

    verifyMonthlyFee(cardType: string, fee: string){
        cy.contains(TEXT_PRICES_AFTER_1_9_2019)
        cy.contains(HEADER_5, TEXT_MONTHLY_FEE)
            .parents(ELEMENT_ARTICLE)
            .find('table tbody').then(tableBody => {
                cy.wrap(tableBody).find('tr').eq(1).then(tableRow => {
                    cy.wrap(tableRow).find('td').then(tableData => {
                        cy.wrap(tableData).first().should('contain', cardType);
                        cy.wrap(tableData).eq(1).contains(fee);
                    })
                })
            })
    }
}

export const nordeaCreditMasterCard = new NordeaCreditMasterCard();

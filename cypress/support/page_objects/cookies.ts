const TEXT_ACCEPT_ALL_BUTTON = 'Hyväksy kaikki';
const TEXT_CONTINUE_WITH_SELECTED_BUTTON = 'Jatka valituilla';

export class Cookies{
    acceptAll() {
        cy.contains(TEXT_ACCEPT_ALL_BUTTON).click();
    }

    continueWithSelected() {
        cy.contains(TEXT_CONTINUE_WITH_SELECTED_BUTTON).click();
    }
}

export const cookie = new Cookies();

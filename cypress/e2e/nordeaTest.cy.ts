import { cookie } from "../support/page_objects/cookies";
import { navigate } from "../support/page_objects/navigation";
import { nordeaCreditMasterCard } from "../support/page_objects/nordeaCreditMasterCard";
import { searchResult } from "../support/page_objects/searchResult";
import * as constants from "../support/page_objects/constants";

const CREDIT_CARD_SEARCH_EXPRESSION= 'Credit Card';

describe('Credit Card Tests', () => {
    beforeEach('Open Site', () => {
        cy.visit('/');
    })

    it('Verify that the monthly fee for the Parallel card is 3,00 €', () => {
        cookie.acceptAll();

        navigate.chooseLanguage(constants.LANGUAGE_ENGLISH);

        navigate.searchItem(CREDIT_CARD_SEARCH_EXPRESSION);

        searchResult.navigateToPageThatContains(constants.NORDEA_CREDIT_MASTERCARD);

        cy.contains(constants.NORDEA_CREDIT_MASTERCARD).click();


        nordeaCreditMasterCard.showPrices();

        nordeaCreditMasterCard.verifyMonthlyFee(
            constants.PARALLEL_CARD, 
            constants.PARALLEL_CARD_MONTHLY_FEE
        );
    })
})
